from twitter.account import Account
import sys


def use_creds(number):
    with open('secrets') as sec:
        tw_accounts = [line.rstrip() for line in sec]
        tw_accounts = [line for line in tw_accounts if line]
        tw_accounts = [line for line in tw_accounts if not line.startswith('#')]
    if number > (len(tw_accounts)):
        sys.exit("Invalid credential number given")
    return tw_accounts[number - 1].split()


def extract_tweets(file):
    try:
        with open(file, 'r') as tw:
            return tw.read().split('\n')
    except IOError:
        sys.exit("There's something wrong with the file that supposedly contains tweets")


def sched_tweets(number, file):
    username, password = use_creds(int(number))
    account = Account(username, password)

    twits = extract_tweets(file)
    for t in twits:
        if t == '':
            continue
        sched, twit, link = t.split('|')
        account.schedule_tweet(' '.join([twit, link]), sched)


def main():
    number, file = sys.argv[1:3]
    sched_tweets(number, file)


if __name__ == "__main__":
    main()
