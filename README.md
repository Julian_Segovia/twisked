# Objectives

* Automate scheduling of tweets
* Minimize online time
* Store tweets for reuse

![twisked screenshot](/static/twiskedscreenshot.png)

## Software requirements
```
$ sudo apt install openssh-client tor python chromium
$ pip install flask tomli
```

# Configure your system

* Prepare the local machine's [ssh connection](/Pablo/Offshore_PRWC_server/wiki/Configure-SSH).
* Install one among the available [\*monkey extensions](/Julian_Segovia/monkey-scripts/src/branch/main/README.md#install-violentmonkey-https-addons-mozilla-org-en-us-firefox-addon-violentmonkey-tampermonkey-https-addons-mozilla-org-en-us-firefox-addon-tampermonkey-greasemonkey-https-addons-mozilla-org-en-us-firefox-addon-greasemonkey-or-firemonkey-https-addons-mozilla-org-en-us-firefox-addon-firemonkey-into-torbrowser) and the [Clipboarding tweetables](/Julian_Segovia/monkey-scripts/src/branch/main/README.md#clipboarding-tweetables) script into TorBrowser.

# Install

* Download a zip package from this repository.
* In a terminal, run after unpacking:

```
$ cd twisked
$ flask run
```
* Access the web browser app at `http://localhost:5000/tweets`
    and start collecting tweets.

# Selling points
* Focus more on the tweet content and less on technical matters
* Execute less steps and and spend less time required to schedule tweets
* Conserve internet bandwidth when you need or are forced to
    * Accomplish tweet scheduling when internet connection is slow
    * Work offline while collecting tweet material
    * Spend the briefest online time possible when sending.
* Let the app do the math of time and date (between UTC and PhTime); use your more superior masscomm talents instead. Provide only the time to begin tweets and interval between tweets, app will automatically:
    * assign date and time for sending each tweet
    * update dates and times when a tweet is moved or deleted, or when tweets are shuffled
    * adjust date and time to match UTC when sending
* Get finer control of tweets' sequence
    * Randomize tweets at the click of a button
    * Manually move any tweet anywhere on the list.
* Use more than one twitter account (only one account per scheduling session)
* Reuse previously posted tweets
* Never get your tweets rejected
    * See realtime character count to help you ensure your tweets don't exceed the maximum
* Pure commandline version available (with less features)
* Get free tickets to a "Concert at the Park" when you buy the product
* Experience stunning GUI interface and animation


# Alternative commandline operations

* Configure your system for ssh
* copy twish into your ~/.local/bin/ directory
```
$ chmod +x ~/.local/bin/twish
```
* If using vim, install twisked plugins (twisked directory) into ~/.vim/pack/plugins/start/
    (to use the plugin, use text file with 'twt' extension to store you tweets)
* Highlight or select text from chosen webpages.
* Paste into text file one tweet material at a time.
* Save the file when collection ends.
* Run twish:
    ` twish <tweet_account_number> <tweets_file> [ | tee -a logfile ] `
* Optional:
    * check the console (where `flask run` was executed)
    * check the website tweets

## FAQ

### Why run preparatory steps on local?
Tweets are prepared for scheduling, sequencing and other tasks on the local machines to provide the user more fine-grained control over the tweets in terms of importance or impact, for instance. This also reduces the need for continuous internet connection especially where such is difficult to acquire, e.g., guerilla war zones.

### Why run the scheduling step on the remote server?
Doing so shortens the process of scheduling in a single short submission event from the point of view of the local computer. The actual scheduling step loops through and submits individual tweets one at a time and would consume online time. Doing this instead on the remote server leverages the network speed and stability unmatched by the miserable state of electronic communication in the Philippines.

### Can this be used on android?
Only the commandline components can be used on android, via termux in particular. The GUI component is still being explored. TorBrowser on android also imposes some restrictions, please see [here](/julian_segovia/monkey-scripts#not_on_android_torbrowser)

### Can images be included in the tweet?
Only if you have a web url for the image. Otherwise, include the tweet into the schedule, and upload-insert the image into the scheduled tweet in the usual web way.

### Why does twisked consider 256 as the character limit when twitter actually states it's 280?
Twitter allots 23 characters for the link/url out of 280. Here is the math:
```
256 text characters + 1 space + 23 for url = 280
```

### Is it compatible with all browsers?
The app has been developed in chromium and, by extension, is expected to work in all chromium-based web-browsers (see full list of [Browsers based on Chromium](https://en.wikipedia.org/wiki/Chromium_(web_browser))). No testing has been done on other browser engines. It has been reported that some features this app use (reading clipboard contents, targetting specific directories for identifying files, etc.) do not yet work in non-chromium browsers.


<!--
<details><summary>

# Technical Specifications
</summary>

## Major components of a running instance
* Remote files
    * python script
    * account configuration file
    * secret account files
* Local scripts and configuration files
    * python-flask script app for gui operations
    * bash script for semi-manual commandline operations
    * configuration files
        * ssh config files
        * twisked config file
        * secret account files
* By-products
    * tweet files
        * temporary for sending: /tmp/tweets.twt
        * for archives: ~/.twisked/tweets/*.twt
            (default filename format: '%Y%m%d-%H%M.twt')
        * tweet file content format:
            ```
            #start:
            #duration:
            #account
            datetime|text|url
            ```
            sample
            ```
            #account:1
            #start:2023-05-12 01:07
            #interval:60
            2023-05-12 01:07|Si Macabuhay ay kusang sumuko matapos talikuran ang kapakanan ng masa at rebolusyonaryong kilusan. Sa kabila ng mga pangako ng estado sa ilalim ng hungkag na E-CLIP, si Macabuhay ay ikinulong sa presinto ng Gubat, Sorsogon na wala pang kasong sinasampa...|https://philippinerevolution.nu/statements/pagkahuli-kay-marlon-macabuhay-ka-gary-pawang-kasinungalingan-ni-pmaj-baltazar-valenzuela/
            2023-05-12 02:07|Notorious for her militarist and fascist mindset, as well as for ambitions to become president, Sara Duterte fits in well in the NTF-Elcac, than in the Department of Education.|https://philippinerevolution.nu/statements/views-about-sara-dutertes-appointment-and-statement-as-ntf-elcac-co-vice-chair/
            ```
        * default format for dates/times inside *.twt: `%Y-%m-%d %H:%M`

## Process details

### Commandline procedures

### GUI

## History

* commandline beginnings on the remote: tweetsked.py
* vim + violentmonkey script; manual scp + ssh command
* commandline script on local: twished
* gui development via flask


# Security

## Routing through TOR

* Ensures anonimity
* Gather tweet text material from philippinerevolution.nu using a Torbrowser. Torbrowser prevents your communication from being tagged by anonymizing your actual IP and by encrypting communication packets.
* Use torservice to access and use the remote server. Torservice wraps ssh tunnels with the same anonymizing technology that is behind the Torbrowser.

</details>

# Troubleshooting

* instead of executing `flask run`, execute
```
flask ‒‒debug run
```

then try to replicate the error. Perhaps error messages will be easier to read. If you know python or javascript, some trace commands can be executed on the web browser's emulated terminal.
* run the web browser's developer tools, eg., inspector, console, etc.
* read the tweet files to inspect if it conforms to the specified format

Why use Flask?
> Because I still don't know other GUI methods or frameworks.

Code/components
* local flask app.
    * has documentation of entire suite
    * keeps a record of:
      * the date-time of last tweet
      * all tweets sent
      * tweets marked for reuse
    * produces text database of tweets for scheduling
* remote python app.
    * produces logs of the actual tweets and errors; for later downloading

==========================================================

# To do
    * change format of start input
    * pin some tweets on top (avoid being included in 'shuffle')
    * display current time in realtime
    * automatic git: pull to import from others, push to report
    * dark mode
    * Use browser-independent gui platform, e.g., Kivy
    * install chrome extension programmatically
        https://superuser.com/a/1650713
        https://askubuntu.com/questions/1063331/how-to-install-google-chrome-extensions-though-terminal
    * using twisked without `clipboarding tweetables`
    * copy tweet files to local server for archiving or for other users
    * separate temporary saves from sent archives
        * actual saving should "ratify" file prior to archiving
    * run browser automatically and open url
    * show results: green check / red cross
        * explain through hover
        * green check; hover: tweet id
        * red cross hover:
            * message rejected (too long)
            * scheduled in the past
            * other (research all possible errors)
        * requires adding field into tweet file
    * update twish
        * to conform with app.py method of creating tweet file
    * resolve: cannot adjust starttime to earlier when importing from future-set tweets
    * include ssh settings in config
        * provide info re use of proper ssh credentials
        * provide info if tor is available
        * warn if neither is available
    * improve tweetsked.py
        * get tweet submission results (eg., success/failure messages, tweet sched id)
        * about rate limiting
    * use ajax to simplify data exchange between front and backend
    * show running count of number of tweets (beside "+")
    * send directly from local computer
        * version for those without access to a remote server
    * use tweeter api token instead of password
    * get followers/followed
    * selectively show control buttons (animmate emergence?)
      * when no entries
          * "+", "import"
      * when one entry
        * plus: table headers, "save", "submit", "clear"
      * 3 or more entries
        * plus: "shuffle"
    * Catch scp error BEFORE proceeding to execute remote script; abort when needed
    * sessions are not saved; click reload or enter at the address bar and data disappears
    * copy row to clipboard ⎘ U+2398
    * other glyphs: 🔀 U+1f500 📷 U+1f4f7 🖫 U+1f5ab 🗐 U+1f5d0 🛈 U+1f6c8 💾 U+1f4be 🐵 U+1f435 📋U+1f4cb 📨U+1f4e8 🖅 U+1f585  🖒 U+1f592 ⭮ U+2b6e ⭯ U+2b6f ⥀ U+2940 ⥁ U+2941 ⟲ U+27f2 ⟳ U+27f3 ⚆ U+2686 ⏲ U+23f2
    * pin: don't include in shuffle
    * Use free remote shells?
        * prerequisite: not using bare password but intead
            * twitter api key or hashed passwords
            * or fetching password from elsewhere
        * both present dangers in a possibly hacker's universe of free shells

* date and time
   * use ntp time: syncs better with twitter; resolves problem when user's system clock is busted

Reference

https://developer.twitter.com/en/docs/api-reference-index
https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-query

https://pypi.org/project/twitter/ (mahirap gamitin)



Find out:
* delete/change scheduling via api
* on android
    * flask operations (including POST actions)
    * clipboard operations via javascript


-->
