from flask import Flask, request
from flask import render_template
import json
from datetime import datetime, timedelta
import tomli
from pathlib import Path
import subprocess
import re

app = Flask(__name__)


def read_config():
    try:
        with open(Path.expanduser(Path('~/.config/twisked/config'))) as cf:
            config = tomli.load(cf)
    except IOError:
        config = {
            'account': 1,
            'tweetsfile_fmt': '%Y%m%d-%H%M.twt',
            'tweetspath': '~/.twisked/tweets',
            'logspath': '~/.twisked/logs',
            'dbpath': '~/twisked/twisked.db',
            'temp_path': '/tmp',
            'temp_file': 'tweets.twt',
            "ssh_secrets": "~/.shecrets",
            'interval': 60,
            'startdelay': 30}
    for p in ["tweetspath", "logspath", "dbpath", "ssh_secrets"]:
        config[p] = Path.expanduser(Path(config[p]))
    if not config["tweetspath"]:
        config["tweetspath"].mkdir(parents=True, exist_ok=True)
    if not config["logspath"]:
        config["logspath"].mkdir(parents=True, exist_ok=True)
    return config


config = read_config()


@app.route('/tweets/', methods=['GET', 'POST'])
def show_tweet_form():
    if request.method == 'GET':
        format = '%Y-%m-%d %H:%M'
        default_startdelay = config['startdelay']
        default_start = (
            datetime.now() + timedelta(minutes=default_startdelay)
            ).strftime(format)
        return render_template(
            'tweets.html',
            start=default_start,
            min=datetime.now().strftime(format),
            interval=config['interval'],
            checked1='checked' if config['account'] == 1 else '',
            checked2='checked' if config['account'] == 2 else '',
            checked3='checked' if config['account'] == 3 else '',
            rows=""
            )
    if request.method == 'POST':
        # tweets, account, action, start, interval
        form_data = json.loads(request.form['formdata']);
        filename = write_tweet_file(form_data);
        if form_data[2] == "send":
            send_tweets(form_data[1], filename)
        form_data = retrieve(filename)
        return render_template(
            'tweets.html',
            start=form_data.get('start').strip(),
            min=form_data.get('start').strip(),
            interval=form_data.get('interval').strip(),
            checked1='checked' if int(form_data.get('account')) == 1 else '',
            checked2='checked' if int(form_data.get('account')) == 2 else '',
            checked3='checked' if int(form_data.get('account')) == 3 else '',
            rows=form_data['rows']
            )


def retrieve(filename):
    with open(Path(config['tweetspath']) / filename, 'r') as tr:
        contents = tr.readlines()
    filedata = {}
    rows = []
    for l in contents:
        if l.startswith("#") and ":" in l:
            k, v = re.sub('^#', '', l).split(':', 1)
            filedata[k] = v
        else:
            row = {}
            row['dt'], row['text'], row['url'] = l.split("|")
            rows.append(row)
    filedata['rows'] = htmlize(rows)
    return filedata


def htmlize(rows):
    delmsg = "Delete this tweet (there is no undelete)."
    countmsg = """
Keep this value green or within the allowable number of characters.
Tweets with texts beyond the max (red) will be rejected by twitter.
"""

    newrow = """
        <div class="row" title="🔗 {url}" draggable="true">
            <div class="move" title="Drag this tweet up or down">🡙</div>
            <div class="dt" contenteditable="true">{dt}</div>
            <div class="text" contenteditable="true" onkeyup="count_chars(this);">{text}</div>
            <div class="url">{url}</div>
            <div class="manage">
                <span class="trash" title="{delmsg}" onClick="del_row(this);">🗑</span><br/>
            </div>
            <span class="count" title="${countmsg}" data-excess="{is_above}">{count}</span>
        </div>
    """
    # must refactor newrow as it duplicates js version
    htmlrow = ""
    for r in rows:
        htmlrow += newrow.format(
            dt=r['dt'],
            text=r['text'],
            url=r['url'],
            count=256-len(r['text']),
            is_above='true' if len(r['text']) > 256 else 'false',
            delmsg=delmsg,
            countmsg=countmsg
            )
    return htmlrow
    """
    resolve rows not acquiring javascript drag listeners
    or python does not return rows as nodes
    when page is repainted by this. 
    "Uncaught TypeError: Failed to execute 'insertBefore' on 'Node': 
    parameter 1 is not of type 'Node'. at HTMLDivElement.dragOver"
    """

def fix_for_utc(date_time):
    format = "%Y-%m-%d %H:%M"
    utc = datetime.strptime(date_time, format) - timedelta(hours=6)
    return utc.strftime(format)
    pass


def write_tweet_file(formdata):
    save_file = datetime.now().strftime(config['tweetsfile_fmt'])
    send_file = Path(config['temp_path']) / 'tweets.twt'
    save_contents = []
    send_contents = []

    save_contents.append("#account:{}".format(formdata[1]))
    save_contents.append("#start:{}".format(formdata[3]))
    save_contents.append("#interval:{}".format(formdata[4]))
    for tweet in formdata[0]:
        save_contents.append("|".join([tweet['dt'], tweet['text'], tweet['url']]))
        send_contents.append("|".join([fix_for_utc(tweet['dt']), tweet['text'], tweet['url']]))
    with open(config['tweetspath']/save_file, 'w') as tf:
        tf.write("\n".join(remove_empties(save_contents)))
    with open(Path(send_file), 'w') as tw:
        tw.write("\n".join(remove_empties(send_contents)))
    return save_file


def remove_empties(list_of_strings):
    list_of_strings = map(str.rstrip, list_of_strings)
    return [s for s in list_of_strings if s]


def read_ssh_secrets():
    with open(config["ssh_secrets"], "r") as sh:
        s = sh.read()
    return tomli.loads(s.replace('=', ' = "').replace('\n', '"\n'))


def send_tweets(account, filename):
    shecrets = read_ssh_secrets()
    # results_file = Path(config['tweetspath'] / filename + '.rsl')
    scp_result = subprocess.run([
        "scp",
        "{}".format(Path(config['temp_path']) / config['temp_file']),
        "{}:{}".format(shecrets['ssh_alias'], shecrets['remote_path'])])
    if scp_result.returncode > 0:
        return "Something went wrong when transmitting the file"
    remote_cmd = "python {} {} {}".format(
        "{}/{}".format(shecrets['remote_path'], "tweetsked.py"),
        account,
        "{}/{}".format(shecrets['remote_path'], config['temp_file']))
    ssh_result = subprocess.run("ssh {} '{}'".format(
            shecrets['ssh_alias'],
            remote_cmd),
        shell=True)
    if ssh_result.returncode > 0:
        return "Something went wrong when executing remote command"


def main():
    show_tweet_form()


if __name__ == "__main__":
    main()
