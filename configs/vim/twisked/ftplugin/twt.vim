set cc=17,273
hi ColorColumn ctermbg=DarkGreen

map <S-Insert> "+p
map! <S-Insert> "+p
nnoremap <S-w> :set wrap!<CR>
