zone_times();
const delmsg = "Delete this tweet (there is no undelete)."
const countmsg = `
Keep this value green or within the allowable number of characters.
Tweets with texts beyond the max (red) will be rejected by twitter.
`
let draggableRow = null;

function add_row(datetime, text, url) {
    var diff = text ? 256 - text.length : 0
    var isExcess = (diff < 0) ? "true" : "false"
    // must refactor newrow as it duplicates python version
    var newrow = `
        <div class="row" title="🔗 ${url}" draggable="true">
            <div class="move" title="Drag this tweet up or down">🡙</div>
            <div class="dt" contenteditable="true">${datetime}</div>
            <div class="text" contenteditable="true" onkeyup="count_chars(this);">${text}</div>
            <div class="url">${url}</div>
            <div class="manage">
                <span class="trash" title="${delmsg}" onClick="del_row(this);">🗑</span><br/>
            </div>
            <span class="count" title="${countmsg}" data-excess="${isExcess}">${diff}</span>
        </div>
    `;
    // 1f5d1 🗑    🡙 U+1f859
    var newrowNode = new DOMParser().parseFromString(newrow, "text/html").body.firstChild
    newrowNode.addEventListener("dragstart", dragStart)
    newrowNode.addEventListener("dragend", dragEnd)
    tbody.appendChild(newrowNode)
    window.scrollTo(0, document.body.scrollHeight)
}

function as_fileinput() {
    input_file.click();
    input_file.onchange = function(){
        input_file.files[0].arrayBuffer().then(function (arrayBuffer) {
            contents = new TextDecoder().decode(arrayBuffer)
        }).then( () => {
            import_past(contents)
        }).then( () => {
            reslate()
        });
    }
}

function del_row(row) {
    deleteRow(row);
    reslate();
}

function deleteRow(row) {
    row.closest(".row").remove();
}

function count_chars(cell) {
    var diff = 256 - cell.innerText.length
    var counter = cell.parentNode.querySelector(".count")
    counter.innerText = diff
    counter.setAttribute('data-excess', (diff < 0) ? "true" : "false")
}

function shuffle_array(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i]
        array[i] = array[j];
        array[j] = temp;
    }
}

function shuffle_rows() {
    var sorted = tbody.querySelectorAll(".row");
    var arrayed = Array.from(sorted);
    shuffle_array(arrayed);
    for (a of arrayed) {
        tbody.append(a);
    }
    reslate();
}

function import_past(contents) {
    var rows = contents.split('\n').filter(no => no[0] !== "#")
    rows.forEach(row => {
        var [dt, text, url] = row.split('|')
        add_row(dt, text, url)
    })
}

function delete_all() {
    tbody.replaceChildren();
}

function submitter(action) {
    var sRows = tbody.querySelectorAll('.row');
    var sTweets = [];
    for (r of sRows.entries()) {
        cells = {};
        for (div of r[1].querySelectorAll('div')) {
            cells[div.className] = div.innerText;
        }
        sTweets.push(cells)
    }
    var sAccount = document.querySelector('input[name="account"]:checked').id[1]
    var sStart = start.querySelector('input').value.replace('T', ' ');
    var sInterval = interval.querySelector('input').value
    response = [sTweets, sAccount, action, sStart, sInterval]
    document.querySelector('#tweet4submit').innerText = JSON.stringify(response)
    submit();
}

// ====================================== Date/Time

function toObjDatetime(strDatetime) {
    return new Date(strDatetime)
}

function toStrDatetime(objDatetime) {
    return new Date(objDatetime).toLocaleString('sv').slice(0,16)
}

function addMinutes(dtObj, minutes) {
    var MSeconds = dtObj.getTime();
    var addMSeconds = minutes * 60 * 1000;
    var newDateObj = new Date(MSeconds + addMSeconds);
    return newDateObj;
}

function reslate() {
    var [rStart, rInterval] = sked_vals();
    var times = tbody.querySelectorAll(".dt");
    if (times.length == 0) {
        return;
    }
    for (let i = 0; i < times.length; i++) {
        times[i].innerText = toStrDatetime(addMinutes(rStart, i * rInterval));
    }
}

function sked_vals() {
    var skedStart = toObjDatetime(start.querySelector('input').value);
    var skedInterval = interval.querySelector('input').value;
    return [skedStart, skedInterval]
}

function zone_times() {
    if (start.querySelectorAll('.tooltip').length > 0) {
        start.removeChild(start.querySelector('span'))
    }
    var startval = start.querySelector('input').value

    var ph = new Date(startval).toLocaleString('sv').slice(0,16)
    var nl = new Date(startval + '+06:00').toISOString().slice(0,16).replace('T', ' ')
    // previous +06:00 actually operates as -06:00, don't ask, IHANI
    var utc = new Date(startval).toISOString().slice(0,16).replace('T', ' ')
    var timecontent = `
        <span class="tooltip">${ph} [PH]<br/>${nl} [NL]<br/>${utc} [UTC]</span>
        `
    start.insertAdjacentHTML("beforeend", timecontent)
}

// ========================================== drag n drop
//
function dragStart(){
    draggableRow = this;
    this.classList.add('dragging')
}

function dragEnd(){
    this.classList.remove('dragging')
    draggableRow = null;
}

function dragOver(e){
    e.preventDefault();
    const afterElement = getDragAfterElement(e.clientY)
    if (afterElement == null) {
        tbody.appendChild(draggableRow)
    } else {
        tbody.insertBefore(draggableRow, afterElement)
    }
    reslate();
}

function getDragAfterElement(y) {
  const draggableElements = [...tbody.querySelectorAll('div[draggable="true"]:not(.dragging)')]

  return draggableElements.reduce((closest, child) => {
    const box = child.getBoundingClientRect()
    const offset = y - box.top - box.height / 2
    if (offset < 0 && offset > closest.offset) {
      return { offset: offset, element: child }
    } else {
      return closest
    }
  }, { offset: Number.NEGATIVE_INFINITY }).element
}

/*
Refs:
To Do App Using HTML, CSS and JavaScript (Drag & Drop)
    https://www.youtube.com/watch?v=m3StLl-H4CY
    (Basir Payenda)
How To Build Sortable Drag & Drop With Vanilla Javascript
    https://www.youtube.com/watch?v=jfYWwQrtzzY
    (Web Dev Simplified)
*/

// ====================================== event listeners

add.addEventListener("click", ()=>{
    var [tstart, tinterval] = sked_vals();
    var numrows = tbody.querySelectorAll('.row').length
    var dt = toStrDatetime(addMinutes(tstart, numrows * tinterval))
    navigator.clipboard.readText().then((copiedText)=>{
        var [d,t,u] = copiedText.split('|');
        add_row(dt, t, u);
    });
});

start.addEventListener("change", ()=>{
    zone_times();
    reslate();
});

interval.addEventListener("change", ()=>{
    reslate();
});

tbody.addEventListener("dragover", dragOver)

